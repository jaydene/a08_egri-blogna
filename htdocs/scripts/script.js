var count = 0;

$(document).ready(function() {
    $(document).ready(CreateBall);
    $("#changeRng").click(colorRng);
    $("#smaller").click(function() {
        $(".circle").css("width", "50px");
        $(".circle").css("height", "50px");
    });
    $("#larger").click(function() {
        $(".circle").css("width", "100px");
        $(".circle").css("height", "100px");
    });
        $("#bounce").click(AnimateBall);
});

function colorRng() {
    var hexValues = "0123456789ABCDEF";
      var hexColors = "#";
      for (var i = 0; i < 6; i++)
       hexColors += hexValues[(Math.floor(Math.random() * 16))];
      $('.circle').css('background', hexColors);
}

function CreateBall() {
  $(".circle").css("height", "100px");
  $(".circle").css("width", "100px");
  $(".circle").css("border-radius", "50%");
  $(".circle").css("margin", "0");
  $(".circle").css("top", "200px");
  $(".circle").css("left", "200px");
  $(".circle").css("position", "relative");
  $("#color").ready(colorRng);
}

function AnimateBall() {
    var ballHt=$(".circle").height();
    var ballW=$(".circle").width();
    var test1 = 1;
    var test2 = 2;
    count++;

    var test = setInterval(function()
    {
        ballHt = $(".circle").height();
        ballW = $(".circle").width();

        if((count % 2) == 1) {
        $(".circle").css("top", function(i, ht) {
            ht = parseInt(ht);
            if (ht+ballHt+1 > $("#displayarea").height() || ht == 0)
                test1 = test1 * -1;
            return ht+test1+"px";
        })

        $(".circle").css("left", function(i, w) {
            w = parseInt(w);
            if (w+ballW+1 > $("#displayarea").width() || w == 0)
                test2 = test2 * -1;
            return w+test2+"px";
        })
      }
      else {
        clearInterval(test);
      }
    }, 20);
}
